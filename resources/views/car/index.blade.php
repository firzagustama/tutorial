@extends('layouts.app')

@section('content')
    <table>
        <thead>
            <tr>
                <td>ID</td>
                <td>User ID</td>
                <td>Brand</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($cars AS $car)
                <tr>
                    <td>{{ $car->id }}</td>
                    <td>{{ $car->user_id }}</td>
                    <td>{{ $car->brand }}</td>
                    <td>
                        <form method="POST" action="{{ route('car.destroy', [$car->id]) }}">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE">
                            <button type="submit">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
