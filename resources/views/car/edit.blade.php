@extends('layouts.app')

@section('content')
<form method="POST" action="{{ route('car.update', [$car->id]) }}">
    @csrf
    <input type="hidden" name="_method" value="PUT">
    <input id="brand" type="text" name="brand" value="{{ $car->brand }}">
    <button type="submit">Simpan</button>
</form>
@endsection
