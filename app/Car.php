<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable = [
        'user_id', 'brand',
    ];

    public function user()
    {
        $this->belongsTo('App\User');
    }
}
