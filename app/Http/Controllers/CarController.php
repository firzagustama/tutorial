<?php

namespace App\Http\Controllers;

use Auth;
use App\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ourwebsite/car METHOD GET
        $cars = Car::all();
        return view('car.index')->with('cars', $cars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // ourwebsite/car/create
        return view('car.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ourwebsite/car METHOD POST
        $user = Auth::user();
        return Car::create([
            'user_id' => $user->id,
            'brand' => $request->input('brand'),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        // ourwebsite/car/CAR_ID METHOD GET
        return $car;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        // ourwebsite/car/CAR_ID/edit
        return view('car.edit')->with('car', $car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        // ourwebsite/car/CAR_ID METHOD POST
        $user = Auth::user();
        $car->user_id = $user->id;
        $car->brand = $request->input('brand');
        $car->save();

        return $car;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        // ourwebsite/car/CAR_ID METHOD DELETE
        $car->delete();
        return $this->index();
    }
}
